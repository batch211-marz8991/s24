const getCube = 2 ** 3;

let message = `The cube of 2 is ${getCube}`
console.log(message)

const address = {
    brgy: "Pobalcion",
    municipality: "Hagonoy",
    province: "Davao del Sur",
    country: "Philippines"
};

const {brgy, municipality, province, country} = address;

let fullAdress = `I lived at ${brgy}, ${municipality}, ${province}, ${country}.`
console.log(fullAdress);



const animal = {
    type: "Dog",
    name: "Spot",
    age: 2,
    breed: "Husky"
};

const {type, name, age, breed} = animal;
let myPet = `My pet is a ${type}, his name is ${name} a ${age} year old ${breed}`
console.log(myPet)


const numbers = [1,2,3,4,5];
numbers.forEach((num) => console.log(num));

const reduceNumber = [1,2,3,4,5];
const reducer = (accu, curr) => accu + curr;
console.log(reduceNumber.reduce(reducer));


class Dog {
    constructor(name,age,breed){
        this.name = name;
        this.age = age;
        this.breed = breed;
    };
}
const myDog = new Dog("Arlow", 3, "Askal");
console.log(myDog);