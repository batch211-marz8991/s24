// console.log("Hello World");

//Es6 updates 

//ES^ isa one of the latest version of writing javascript

//let , const are ES6 updates

//Exponent operator(**)

const firtsNum = 8 ** 2;
console.log(firtsNum);

const secondNum = Math.pow(8,2);
console.log(secondNum);

let string1 = "fun";
let string2 = "Bootcamp";
let string3 = "Coding";
let string4 = "Javascript";
let string5 = "Zuitt";
let string6 = "Learning";
let string7 = "love";
let string8 = "I";
let string9 = "is";
let string10 = "in";

// let sentence1 = string8 + " " + string7 + " " + string5 + " " + string3 + " " + string2+ ".";
// let sentence2 = string6 + " " + string4 + " " + string9 + " " + string1 + ".";
// console.log(sentence1);
// console.log(sentence2);


//"" , '' - string literals

//Template literals
        //Allow us to create strings `` and eaasily embed javascript expresion in it


let sentence1 = `${string8} ${string7} ${string5} ${string3} ${string2}!`;
console.log(sentence1);

//%{} - used to embed js expression when creating string

//pre-template literal string

let message = "Hello" + name + '! Welcome to programming!';

message = `Hello ${name}! Welcome to programming.`;

const anotherMessage = `${name} attended a math compitition. 
He won it by solving the problem 8 ** 2 with the solution of ${firtsNum}.`
console.log(anotherMessage);


let dev = {
    name: "Peter",
    lastName: "Parker",
    occupation: "web developer",
    income: 50000,
    expenses: 60000
};

console.log(`${dev.name} is a ${dev.occupation}.`)
console.log(`His income is ${dev.income} and his expenses at ${dev.expenses}. His current balance is ${dev.income - dev.expenses}.`);

const interestRate = .1;
const principal = 1000;

console.log(`the interest on your account is ${principal * interestRate}.`);

const fullName = ["Juan", "Dela", "Cruz"];
console.log(fullName[0])
console.log(fullName[1])
console.log(fullName[2])

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It is nice to meet you!`);

const [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);


console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It is nice to meet you!`);

//object destructuring - allows us unpact properties from object into distinc variables

const person = {
    givenName: "Jane",
    maidenName: "Dela",
    familyName: "Cruz"
};


//pre-object destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);


//object destructuring

const {givenName, maidenName, familyName} = person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);

//Arrow functions - alternative syntax

const hello = () => {
    console.log("Hello Wolrd");
}
hello()

// const hello = function hello(){
//     console.log("Hello World");
// }

// hello()

//pre-arrow function and template literals


//Arrow function 
// let/const variablename = (parameterA,parameterB,parameterC,){
//    console.log()
//}

const printFullName = (firstName, middleInitial,lastName) => {
    console.log(`${firstName} ${middleInitial} ${lastName}`)
};

printFullName("John","D.","Smith");


//Arrow functions with loops

const student = ["John", "Jane", "Judy"];



student.forEach(function(student){
    console.log(`${student} is a student`)
});

//arrow function

student.forEach((student)=>{
    console.log(`${student} is a student`)
});


//implicit return statement 


// const add = (x,y) =>{
//     return x + y;
// }

// let total = add(1,2);
// console.log(total);



const add = (x,y) => x + y;

let total = add(1,2);
console.log(total);


const subtract = (x,y) => x - y;
let deference = subtract(1,2);
console.log("the result of subtract function is " + deference);

const multiply = (x,y) => x * y;
let product = multiply(1,2);
console.log("the result of multiply function is " + product);


const devide = (x,y) => x / y;
let qoutient = devide(1,2);
console.log("the result of devide function is " + qoutient);


//default function argument value

const greet = (name = "User") =>{
    return `Good evening, ${name}`
};

console.log(greet());
console.log(greet("John"));



//class-based object blueprint

//creating a class

class Car {
    constructor(brand,name,year){
        this.brand = brand;
        this.name = name;
        this.year = year;
    }
}

//instantiate an object

const myCar = new Car();
console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Everest";
myCar.year = 1996;

console.log(myCar);

const myNewCar = new Car("toyota", "Vios", 2021);
console.log(myNewCar);


class Character {
    constructor(name,role,weakness,strenght){
        this.name = name;
        this.role = role;
        this.weakness = weakness;
        this.strenght = strenght;
    }
}

const NewCharacter1 = new Character("Gon", "fighter", "soft hearted", "powerfull");
const NewCharacter2 = new Character("killua", "assasin", "none", "speed");

console.log(NewCharacter1);
console.log(NewCharacter2);
